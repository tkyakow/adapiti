<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Assunto extends Model {

	protected $fillable = ["titulo"];

	public function disciplinas(){
		return $this->belongsToMany("App\Disciplina");
	}

	public function conteudos(){
		return $this->hasMany("App\Conteudo");
	}
}
