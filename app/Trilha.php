<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Trilha extends Model {

	protected $fillable = ['titulo'];

    public function agrupamentos(){
        return $this->belongsToMany("App\Agrupamento");
    }

    public function cursos(){
        return $this->belongsToMany("App\Curso");
    }

    public function getAgrupamentoListAttribute(){
    	return $this->agrupamentos()->select('agrupamento_id')->lists('agrupamento_id');
    }

    public function getCursoListAttribute(){
    	return $this->cursos()->select('curso_id')->lists('curso_id');
    }

}
