<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model {

	protected $fillable = ["url","conteudo_id"];

	public function conteudos(){
		return $this->belongsTo("App\Conteudo");
	}
}
