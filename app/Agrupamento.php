<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Agrupamento extends Model {

	protected $fillable = ['titulo'];

    public function trilha(){
    	return $this->belongsToMany('App\Trilha');
    }

	public function disciplinas(){
		return $this->belongsToMany("App\Disciplina");
	}

	public function getDisciplinaListAttribute(){
    	return $this->disciplinas()->select('disciplina_id')->lists('disciplina_id');
    }


}
