<?php namespace App\Http\Controllers;

use App\Disciplina;
use App\Assunto;
use App\Agrupamento;

use Lang;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\DisciplinaRequest;

class DisciplinaController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id = null)
	{
		if($id != null){
			$disciplinas = Disciplina::whereHas('assuntos', function ($q) use($id){
				$q->where('assunto_disciplina.assunto_id', '=', $id);
			})->paginate(10);
		} else {
			$disciplinas = Disciplina::with("assuntos")->paginate(10);
		}
		$assuntos = Assunto::lists("titulo","id");
		return view("disciplina.index",compact("disciplinas","assuntos"));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$assuntos = Assunto::lists("titulo","id");
		return view("disciplina.create",["assuntos" => $assuntos]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(DisciplinaRequest $request)
	{ 
		$disciplina = Disciplina::create($request->all());

		if($this->syncAssuntos($disciplina,$request->input('assuntos_list'))){
		} else {
			Session::flash('flash_message',Lang::get('flash.cadastrado_erro'));
		}
		return redirect('disciplina');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$disciplina = Disciplina::findOrFail($id);
		$assuntos = Assunto::lists('titulo','id');
		return view("disciplina.edit",compact('disciplina','assuntos'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(DisciplinaRequest $request,$id)
	{
		$disciplina = Disciplina::findOrFail($id);
		$this->syncAssuntos($disciplina,$request->input('assuntos_list',[]));
		if($disciplina->update($request->all())){
			Session::flash('flash_message',Lang::get('flash.cadastrado'));
		} else {
			Session::flash('flash_message',Lang::get('flash.cadastrado_erro'));
		}
		return redirect('disciplina');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Disciplina::find($id)->delete()){
			Session::flash('flash_message',Lang::get('flash.deletado'));
		} else {
			Session::flash('flash_message',Lang::get('flash.deletado_erro'));
		}
		return redirect('disciplina');
	}

    public function syncAssuntos(Disciplina $disciplina, array $assuntos){
        if($disciplina->assuntos()->sync($assuntos)){
        	return true;
        } else {
        	return false;
        }
    }

}
