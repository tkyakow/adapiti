<?php namespace App\Http\Controllers;

use App\Agrupamento;
use App\Disciplina;
use App\Trilha;
use Lang;
use Input;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Illuminate\Http\Request;
use App\Http\Requests\AgrupamentoRequest;

class AgrupamentoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$agrupamentos = Agrupamento::paginate(10);
		return view('agrupamento.index',['agrupamentos' => $agrupamentos]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$disciplinas = Disciplina::lists("titulo","id");
		return view('agrupamento.create',['disciplinas' => $disciplinas]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AgrupamentoRequest $request)
	{
		$agrupamento = Agrupamento::create($request->all());

		if($this->syncDisciplinas($agrupamento,$request->input('disciplina_list',[]))){
			Session::flash('flash_message',Lang::get('flash.cadastrado'));
		} else {
			Session::flash('flash_message',Lang::get('flash.cadastrado_erro'));
		}
		return redirect('agrupamento');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{ 
		$disciplinas = Disciplina::lists('titulo', 'id');
		$agrupamento = Agrupamento::findOrFail($id);
		return view('agrupamento.edit',compact('disciplinas','agrupamento'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, AgrupamentoRequest $request)
	{
		$agrupamento = Agrupamento::findOrFail($id);
		$this->syncDisciplinas($agrupamento,$request->input('disciplina_list',[]));
		if($agrupamento->update($request->all())){
        	Session::flash('flash_message',Lang::get('flash.atualizado'));
        } else {
        	Session::flash('flash_message',Lang::get('flash.atualizado_erro'));
        }
        return redirect('agrupamento');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Agrupamento::find($id)->delete()){
			Session::flash('flash_message',Lang::get('flash.deletado'));
		} else {
			Session::flash('flash_message',Lang::get('flash.deletado_erro'));
		}
		return redirect('agrupamento');
	}

	function sort(){
		$agrupamentos = Agrupamento::orderBy('sort_order')->get();
		return view("agrupamento.sort",compact("agrupamentos"));
	}

	function sort_ajax(){
		$ids = explode(',',Input::get('sort_order'));
		foreach($ids as $index => $id):
			echo $id;
			$agrupamento = Agrupamento::findOrFail($id);
			$agrupamento->sort_order = $index+1;
			$agrupamento->save();
		endforeach;
	}

    public function syncDisciplinas(Agrupamento $agrupamento, array $disciplina){
        if($agrupamento->disciplinas()->sync($disciplina)){
        	return true;	
        } else {
        	return false;
        }
        
    }

    public function aberto($id){	
		$agrupamentos = Agrupamento::whereHas('trilha', function ($q) use($id){
			$q->where('agrupamento_trilha.trilha_id', '=', $id);
		})->orderBy('sort_order')->get();	
		
		$trilha = Trilha::find($id);

    	return view('agrupamento.aberto',compact('agrupamentos','trilha'));
    }

}