<?php namespace App\Http\Controllers;

use App\Conteudo;
use App\Disciplina;
use Response;
use App\Assunto;
use App\Video;
use App\File;
use Input;
use Lang;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ConteudoRequest;

class ConteudoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id = null)
	{
		if($id != null){
			$conteudos = Conteudo::with("assuntos")->where('assunto_id', '=', $id)->paginate(10);
		} else {
			$conteudos = Conteudo::with("assuntos")->paginate(10);
		}

		$assuntos = Assunto::lists("titulo","id");
		return view("conteudo.index",compact("conteudos","assuntos"));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$assuntos = Assunto::lists("titulo","id");
		$videos = Video::lists("url","id");
		return view("conteudo.create",compact("videos","assuntos"));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ConteudoRequest $request)
	{
		$conteudo = Conteudo::create($request->all());

		foreach($request->input('url_list') as $req):
			$conteudo->videos()->create(["url" => $req]);
		endforeach;

		if($conteudo){
			Session::flash('flash_message',Lang::get('flash.cadastrado'));
		} else {
			Session::flash('flash_message',Lang::get('flash.cadastrado_erro'));
		}
		return redirect('conteudo');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$videos = Video::lists("url","id");
		$conteudo = Conteudo::with("files")->findOrFail($id);
		$assuntos = Assunto::lists('titulo','id');
	
		return view("conteudo.edit",compact('conteudo','videos','assuntos'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ConteudoRequest $request,$id)
	{
		if(Conteudo::findOrFail($id)->update($request->all())){
        	Session::flash('flash_message',Lang::get('flash.atualizado'));
        } else {
        	Session::flash('flash_message',Lang::get('flash.atualizado_erro'));
        }
        return redirect('conteudo');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Conteudo::find($id)->delete()){
			Session::flash('flash_message',Lang::get('flash.deletado'));
		} else {
			Session::flash('flash_message',Lang::get('flash.deletado_erro'));
		}
        return redirect('conteudo');
	}

	public function upload(){
		$file = \Request::file("arquivo");
		$conteudoId = \Request::get("conteudoId");

		$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

		$storage_path = storage_path().'/documentos';
		$extension = $file->getClientOriginalExtension(); // getting image extension
      	$fileName = md5($randomString).'.'.$extension; // renameing image

		$fileModel = new \App\File();
		$fileModel->name = $fileName;
		$fileModel->conteudo_id = $conteudoId;

		$fileModel->save();

		return $file->move($storage_path, $fileName);
	}

	public function deletar_arquivo($conteudoId,$fileId){
		$file = \App\File::find($fileId);

		$storage_path = storage_path().'/documentos';

		$file->delete();

		if(unlink($storage_path."/".$file->name)){
			Session::flash('flash_message',Lang::get('flash.deletado'));
		} else {
			Session::flash('flash_message',Lang::get('flash.deletado_erro'));
		}
        return redirect()->back();
	}

	public function aberto($id){
		$conteudos = Conteudo::with("files")->with("videos")->findOrFail($id);
		return view("conteudo.aberto",["conteudos" => $conteudos]);
	}

	public function adicionar_video(){
		$conteudo = Conteudo::find(Input::get('conteudo_id'));
		$conteudo->videos()->create(array('conteudo_id' => Input::get('conteudo_id') , 'url' => Input::get('url')));
		$videos = Conteudo::find(Input::get('conteudo_id'))->videos()->where('conteudo_id', '=', Input::get('conteudo_id'))->get();
		return Response::json($videos);
	}

	public function deletar_video(){
		Video::find(Input::get('id'))->delete(Input::get('id'));
		$videos = Conteudo::find(Input::get('conteudo_id'))->videos()->where('conteudo_id', '=', Input::get('conteudo_id'))->get();
		return Response::json($videos);
	}
}
