<?php namespace App\Http\Controllers;

use App\Exemplo;
use Lang;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ExemploRequest;
class ExemploController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$exemplos = Exemplo::paginate(10);
		return view('exemplo.index',['exemplos' => $exemplos]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('exemplo.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ExemploRequest $request)
	{
		if(Exemplo::create($request->all())){
			Session::flash('flash_message',Lang::get('flash.cadastrado'));
		} else {
			Session::flash('flash_message',Lang::get('flash.cadastrado_erro'));
		}
		return redirect('exemplo');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$exemplo = Exemplo::findOrFail($id);
		return view('exemplo.edit',['exemplo'=>$exemplo]);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, ExemploRequest $request)
	{
        if(Exemplo::find($id)->update($request->all())){
        	Session::flash('flash_message',Lang::get('flash.atualizado'));
        } else {
        	Session::flash('flash_message',Lang::get('flash.atualizado_erro'));
        }
        return redirect('exemplo');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Exemplo::find($id)->delete()){
			Session::flash('flash_message',Lang::get('flash.deletado'));
		} else {
			Session::flash('flash_message',Lang::get('flash.deletado_erro'));
		}
		return redirect('exemplo');
	}

}
