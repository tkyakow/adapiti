<?php namespace App\Http\Controllers;

use App\Trilha;
use App\Agrupamento;
use App\Curso;
use Lang;
use Input;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\TrilhaRequest;

class TrilhaController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$trilhas = Trilha::paginate(10);
		return view("trilha.index",["trilhas" => $trilhas]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$agrupamentos = Agrupamento::lists("titulo","id");
		$cursos = Curso::lists("titulo","id");
		return view("trilha.create",compact('agrupamentos','cursos'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(TrilhaRequest $request)
	{
		$trilha = Trilha::create($request->all());

		if($this->syncAgrupamentos($trilha,$request->input('agrupamento_list',[])) && $this->syncCursos($trilha,$request->input('curso_list',[]))){
			Session::flash('flash_message',Lang::get('flash.cadastrado'));
		} else {
			Session::flash('flash_message',Lang::get('flash.cadastrado_erro'));
		}
		return redirect('trilha');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$agrupamentos = Agrupamento::lists("titulo","id");
		$cursos = Curso::lists("titulo","id");
		$trilha = Trilha::findOrFail($id);
		return view("trilha.edit",compact('trilha','agrupamentos','cursos'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, TrilhaRequest $request)
	{
		$trilha = Trilha::findOrFail($id);
		$this->syncAgrupamentos($trilha,$request->input('agrupamento_list',[]));
		$this->syncCursos($trilha,$request->input('curso_list',[]));
		if($trilha->update($request->all())){
        	Session::flash('flash_message',Lang::get('flash.atualizado'));
        } else {
        	Session::flash('flash_message',Lang::get('flash.atualizado_erro'));
        }
        return redirect('trilha');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Trilha::find($id)->delete()){
			Session::flash('flash_message',Lang::get('flash.deletado'));
		} else {
			Session::flash('flash_message',Lang::get('flash.deletado_erro'));
		}
		return redirect('trilha');
	}


    public function syncAgrupamentos(Trilha $trilha, array $agrupamentos){
        if($trilha->agrupamentos()->sync($agrupamentos)){
        	return true;
        } else {
        	return false;
        }
    }

    public function syncCursos(Trilha $trilha, array $cursos){
        if($trilha->cursos()->sync($cursos)){
        	return true;
        } else {
        	return false;
        }
    }

    public function aberta(){
		$trilhas = Trilha::orderBy('sort_order')->get();
		return view("trilha.aberta",["trilhas" => $trilhas]);
    }

	function sort(){
		$trilhas = Trilha::orderBy('sort_order')->get();
		return view("trilha.sort",compact("trilhas"));
	}

	function sort_ajax(){
		$ids = explode(',',Input::get('sort_order'));
		foreach($ids as $index => $id):
			$trilha = Trilha::findOrFail($id);
			$trilha->sort_order = $index+1;
			$trilha->save();
		endforeach;
	}

}
