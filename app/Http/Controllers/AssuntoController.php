<?php namespace App\Http\Controllers;

use App\Assunto;
use App\Disciplina;
use Lang;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\AssuntoRequest;

class AssuntoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$assuntos = Assunto::paginate(10);
		return view("assunto.index",["assuntos" => $assuntos]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view("assunto.create");
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AssuntoRequest $request)
	{
		if(Assunto::create($request->all())){
			Session::flash('flash_message',Lang::get('flash.cadastrado'));
		} else {
			Session::flash('flash_message',Lang::get('flash.cadastrado_erro'));
		}
		return redirect('assunto');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$assunto = Assunto::findOrFail($id);
		return view("assunto.edit",["assunto" => $assunto]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(AssuntoRequest $request, $id)
	{
		if(Assunto::findOrFail($id)->update($request->all())){
        	Session::flash('flash_message',Lang::get('flash.atualizado'));
        } else {
        	Session::flash('flash_message',Lang::get('flash.atualizado_erro'));
        }
        return redirect('assunto');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Assunto::find($id)->delete()){
			Session::flash('flash_message',Lang::get('flash.deletado'));
		} else {
			Session::flash('flash_message',Lang::get('flash.deletado_erro'));
		}
        return redirect('assunto');
	}

	public function aberto($id){
		$assuntos = Assunto::whereHas('disciplinas', function ($q) use($id){
			$q->where('assunto_disciplina.disciplina_id', '=', $id);
		})->get();

		$disciplinas = Disciplina::find($id);

    	return view('assunto.aberto',compact('assuntos','disciplinas'));
	}

}
