<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post("upload" , ["as" => "files.upload" , "uses" => "ConteudoController@upload"]);
Route::get("remover/{conteudoId}/{fileId}" , ["as" => "files.destroy" , "uses" => "ConteudoController@deletar_arquivo"]);

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::resource('user','UserController');

Route::group(['prefix'=>'exemplo'], function() {

    Route::get('',['as'=>'exemplo',                        'uses'=>'ExemploController@index']);
    Route::get('create',['as'=>'exemplo.create',           'uses'=>'ExemploController@create']);
    Route::post('store',['as'=>'exemplo.store',            'uses'=>'ExemploController@store']);

    Route::get('{id}/destroy',['as'=>'exemplo.destroy',    'uses'=>'ExemploController@destroy']);
    Route::get('{id}/edit',['as'=>'exemplo.edit',          'uses'=>'ExemploController@edit']);
    Route::put('{id}/update',['as'=>'exemplo.update',      'uses'=>'ExemploController@update']);
 });

Route::group(['prefix'=>'trilha'], function() {

    Route::get('',['as'=>'trilha',                        'uses'=>'TrilhaController@index']);
    Route::get('aberta',['as'=>'trilha.aberta',           'uses'=>'TrilhaController@aberta']);
    Route::get('create',['as'=>'trilha.create',           'uses'=>'TrilhaController@create']);
    Route::post('store',['as'=>'trilha.store',            'uses'=>'TrilhaController@store']);

    Route::get('{id}/destroy',['as'=>'trilha.destroy',    'uses'=>'TrilhaController@destroy']);
    Route::get('{id}/edit',['as'=>'trilha.edit',          'uses'=>'TrilhaController@edit']);
    Route::put('{id}/update',['as'=>'trilha.update',      'uses'=>'TrilhaController@update']);

    Route::get('sort',['as'=>'trilha.sort',               'uses'=>'TrilhaController@sort']);
    Route::post('sort_ajax',['as'=>'trilha.sort_ajax',    'uses'=>'TrilhaController@sort_ajax']);
 });

Route::group(['prefix'=>'agrupamento'], function() {

    Route::get('',['as'=>'agrupamento',                        'uses'=>'AgrupamentoController@index']);
    Route::get('create',['as'=>'agrupamento.create',           'uses'=>'AgrupamentoController@create']);
    Route::post('store',['as'=>'agrupamento.store',            'uses'=>'AgrupamentoController@store']);

    Route::get('{id}/destroy',['as'=>'agrupamento.destroy',    'uses'=>'AgrupamentoController@destroy']);
    Route::get('{id}/edit',['as'=>'agrupamento.edit',          'uses'=>'AgrupamentoController@edit']);
    Route::put('{id}/update',['as'=>'agrupamento.update',      'uses'=>'AgrupamentoController@update']);
    Route::get('{id}/aberto',['as'=>'agrupamento.aberto',      'uses'=>'AgrupamentoController@aberto']);

    Route::get('sort',['as'=>'agrupamento.sort',               'uses'=>'AgrupamentoController@sort']);
    Route::post('sort_ajax',['as'=>'agrupamento.sort_ajax',    'uses'=>'AgrupamentoController@sort_ajax']);
 });

Route::group(['prefix'=>'disciplina'], function() {

    Route::get('create',['as'=>'disciplina.create',           'uses'=>'DisciplinaController@create']);
    Route::post('store',['as'=>'disciplina.store',            'uses'=>'DisciplinaController@store']);

    Route::get('{id}/destroy',['as'=>'disciplina.destroy',    'uses'=>'DisciplinaController@destroy']);
    Route::get('{id}/edit',['as'=>'disciplina.edit',          'uses'=>'DisciplinaController@edit']);
    Route::put('{id}/update',['as'=>'disciplina.update',      'uses'=>'DisciplinaController@update']);
    Route::get('{id?}',['as'=>'disciplina',                   'uses'=>'DisciplinaController@index']);
 });

Route::group(['prefix'=>'assunto'], function() {

    Route::get('',['as'=>'assunto',                        'uses'=>'AssuntoController@index']);
    Route::get('create',['as'=>'assunto.create',           'uses'=>'AssuntoController@create']);
    Route::post('store',['as'=>'assunto.store',            'uses'=>'AssuntoController@store']);

    Route::get('{id}/destroy',['as'=>'assunto.destroy',    'uses'=>'AssuntoController@destroy']);
    Route::get('{id}/edit',['as'=>'assunto.edit',          'uses'=>'AssuntoController@edit']);
    Route::put('{id}/update',['as'=>'assunto.update',      'uses'=>'AssuntoController@update']);
    Route::get('{id}/aberto',['as'=>'assunto.aberto',      'uses'=>'AssuntoController@aberto']);
 });

Route::group(['prefix'=>'conteudo'], function() {

    Route::get('create',['as'=>'conteudo.create',           'uses'=>'ConteudoController@create']);
    Route::post('store',['as'=>'conteudo.store',            'uses'=>'ConteudoController@store']);
    Route::post('adicionar_video',['as'=>'conteudo.adicionar_video', 'uses'=>'ConteudoController@adicionar_video']);
    Route::post('deletar_video',['as'=>'conteudo.deletar_video', 'uses'=>'ConteudoController@deletar_video']);

    Route::get('{id}/destroy',['as'=>'conteudo.destroy',    'uses'=>'ConteudoController@destroy']);
    Route::get('{id}/edit',['as'=>'conteudo.edit',          'uses'=>'ConteudoController@edit']);
    Route::put('{id}/update',['as'=>'conteudo.update',      'uses'=>'ConteudoController@update']);
    Route::get('{id}/aberto',['as'=>'conteudo.aberto',      'uses'=>'ConteudoController@aberto']);
    Route::get('{id?}',['as'=>'conteudo.index',                   'uses'=>'ConteudoController@index']);

 });

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	'listar' => 'Auth\AuthController',
]);