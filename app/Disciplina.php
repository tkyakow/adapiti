<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Disciplina extends Model {

	protected $fillable = ['titulo'];

	public function assuntos(){
		return $this->belongsToMany("App\Assunto");
	}

	public function getAssuntosListAttribute(){
    	return $this->assuntos()->select('assunto_id')->lists('assunto_id');
    }

}
