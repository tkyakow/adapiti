<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Exemplo extends Model {

	protected $fillable = ['titulo','descricao'];

}
