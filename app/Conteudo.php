<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Conteudo extends Model {

	protected $fillable = ["titulo","conteudo","assunto_id"];

	public function videos(){
		return $this->hasMany("App\Video", 'conteudo_id')->latest();
	}

	public function files(){
		return $this->hasMany("App\File");
	}

	public function assuntos(){
		return $this->belongsTo("App\Assunto", 'assunto_id');
	}

	public function getUrlListAttribute(){
		return $this->videos()->select("id")->lists("id");
	}

	public function getAssuntoListAttribute(){
		return $this->assuntos()->select("id")->lists("id");
	}
}
