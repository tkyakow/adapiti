<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConteudosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conteudos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->text('conteudo');
			$table->integer('ativo');
			$table->timestamps();

            $table->integer('assunto_id')->unsigned();
            $table->foreign('assunto_id')->references('id')->on('assuntos')->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conteudos');
	}

}
