<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgrupamentoTrilhaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('agrupamento_trilha', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('agrupamento_id')->unsigned();
            $table->foreign('agrupamento_id')->references('id')->on('agrupamentos')->onDelete('cascade');

            $table->integer('trilha_id')->unsigned();
            $table->foreign('trilha_id')->references('id')->on('trilhas')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('agrupamento_trilha');
	}

}
