<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Adapiti concluinte</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<?php
	/*
		* O Conteudo é de um assunto
		* Uma disciplina, é composta por vários assuntos
		* Um agrupamento é composto por várias disciplinas
		* Uma trilha é composta de vários agrupamentos
	*/
	?>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
	
	<link href="{{ asset('/css/ekko-lightbox.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/summernote.css') }}" rel="stylesheet">
	<link href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet">

	<link href="{{ asset('/jquery-file-uploads/css/jquery.fileupload.css') }}" rel="stylesheet">

	{!! Html::script('js/ekko-lightbox.js') !!}
	{!! Html::script('js/summernote.js') !!}

	{!! Html::script('jquery-file-uploads/js/vendor/jquery.ui.widget.js') !!}

	{!! Html::script('jquery-file-uploads/js/jquery.fileupload.js') !!}

</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="">Adapiti concluinte</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{ url('/user') }}">Usuários</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Módulos<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ url('/trilha') }}">1. Trilha</a></li>
							<li><a href="{{ url('/agrupamento') }}">1.1 Agrupamento</a></li>
							<li><a href="{{ url('/disciplina') }}">1.2 Disciplina</a></li>
							<li><a href="{{ url('/assunto') }}">1.3 Assunto</a></li>
							<li><hr /></li>
							<li><a href="{{ url('/conteudo') }}">2.0 Conteúdo</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Aluno<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ url('/trilha/aberta') }}">Trilha</a></li>
						</ul>
					</li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Registrar</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Sair</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@if(Session::has('flash_message'))
		<div class="alert alert-success">{{ Session::get('flash_message') }}</div>
	@endif

	@yield('content')
	@yield('javascript')

</body>
</html>
