@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Lista de trilhas</div>
				<div class="panel-body">
					<a href=" {{ url('/trilha/create') }}" class="btn btn-primary">Novo registro</a>
					<a href=" {{ url('/trilha/sort') }}" class="btn btn-warning">Ordenar registro(s)</a>
					<br />
					<br />
					<table class="table table-striped table-bordered table-hover">
					    <thead>
					        <tr>
					            <th>ID</th>
					            <th>Título</th>
					            <th>Agrupamento(s)</th>
					            <th>Curso(s)</th>
					            <th>Ação</th>
					        </tr>
					    </thead>
					    @foreach($trilhas as $trilha)
			                <tr>
			                    <td>{{ $trilha->id }}</td>
			                    <td>{{ $trilha->titulo }}</td>
			                    <td>
			                    	@foreach($trilha->agrupamentos as $agrupamento)
			                    		{{ $agrupamento->titulo }}
			                    		{!! "<br />" !!}
			                    	@endforeach
			                    </td>
			                    <td>
			                    	@foreach($trilha->cursos as $curso)
			                    		{{ $curso->titulo }}
			                    		{!! "<br />" !!}
			                    	@endforeach
			                    </td>
			                    <td>
			                        <a href="{{ URL::route('trilha.edit',['id'=>$trilha->id])  }}" class="btn btn-success">Editar</a>
			                        <a href="{{ URL::route('trilha.destroy',['id'=>$trilha->id])  }}" class="btn btn-danger">Deletar</a>
			                    </td>
			                </tr>
			            @endforeach
					</table>

					 {!! $trilhas->render() !!}

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
