@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Passo 1:</strong> Escolha uma trilha</div>
				<div class="panel-body">
					<div class="row">
						@foreach($trilhas as $trilha)
							<div class="col-lg-4" style="min-height:200px">
								<div class="panel panel-default">
									<div class="panel-body">
									  <h2>{{ $trilha->titulo }}</h2>
									</div>
									<div class="panel-footer">
			                            <p><a href="{{ URL::route('agrupamento.aberto',['id'=>$trilha->id])  }}" role="button" href="#" class="btn btn-primary btn-lg btn-block">Começar »</a></p>
			                        </div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
