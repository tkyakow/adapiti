<div class="form-group">
	{!! Form::label('titulo','Título'); !!}
	{!! Form::text('titulo',null,['class'=>'form-control']); !!}
</div>

<div class="form-group">
	{!! Form::label('agrupamento_list','Agrupamento(s):'); !!}
	{!! Form::select('agrupamento_list[]', $agrupamentos , null, ['id' => 'agrupamento_list','class'=>'form-control','multiple']); !!}
</div>

<div class="form-group">
	{!! Form::label('curso_list','Curso(s):'); !!}
	{!! Form::select('curso_list[]', $cursos , null, ['id' => 'curso_list','class'=>'form-control','multiple']); !!}
</div>

<div class="form-group">
	{!! Form::submit($submitButtonText,['class'=>'btn btn-primary form-control']) !!}
</div>

<script>
	$('#agrupamento_list').select2({
		placeholder: "Escolha um ou vários agrupamento(s)"
	});
	$('#curso_list').select2({
		placeholder: "Escolha um ou vários curso(s)"
	});
</script>