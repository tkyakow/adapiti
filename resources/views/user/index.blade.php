@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Lista de usuários</div>
				<div class="panel-body">
					<a href=" {{ url('/user/create') }}" class="btn btn-primary">Novo registro</a>
					<br />
					<br />
					<table class="table table-striped table-bordered table-hover">
					    <thead>
					        <tr>
					            <th>ID</th>
					            <th>Nome</th>
					            <th>Grupos</th>
					            <th>Ação</th>
					        </tr>
					    </thead>
					    @foreach($users as $user)
			                <tr>
			                    <td>{{ $user->id }}</td>
			                    <td>{{ $user->name }}</td>
			                    <td>{{ $user->name }}</td>
			                    <td>
			                        <a href=" " class="btn btn-success">Editar</a>
			                        <a href=" " class="btn btn-danger">Deletar</a>
			                    </td>
			                </tr>
			            @endforeach
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
