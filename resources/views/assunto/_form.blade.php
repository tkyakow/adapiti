<div class="form-group">
	{!! Form::label('titulo','Título'); !!}
	{!! Form::text('titulo',null,['class'=>'form-control']); !!}
</div>

<div class="form-group">
	{!! Form::submit($submitButtonText,['class'=>'btn btn-primary form-control']) !!}
</div>

<script>
	$('#disciplina_list').select2({
		placeholder: "Escolha uma ou várias discplina(s)."
	});
</script>