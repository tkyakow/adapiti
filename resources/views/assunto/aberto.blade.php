@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3>{{ $disciplinas->titulo }}</h3>
					<strong>Passo 3:</strong> Escolha um assunto e um conteúdo que pertence a esse assunto
				</div>
				<div class="panel-body">
					<hr />
						{!! Form::button('Voltar', array('id' => 'voltar' , "class" => "btn btn-warning")) !!}
					<hr />
					<div class="row">
						@foreach($assuntos as $assunto)
							<div class="col-lg-4">
								<div class="panel panel-primary">
			                        <div class="panel-heading">
			                            {{ $assunto->titulo }}
			                        </div>
			                        <div class="panel-body">
			                        	<ul>
					                    	@foreach($assunto->conteudos as $conteudo)
					                    		<li>
					                    			<a href="{{ URL::route('conteudo.aberto',['id'=>$conteudo->id])  }}">
					                    				{{ $conteudo->titulo }}
					                    			</a>
					                    		</li>
					                    	@endforeach
				                    	</ul>
			                        </div>
			                    </div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$( "#voltar" ).click(function() {
	  history.back()
	});
</script>


@endsection
