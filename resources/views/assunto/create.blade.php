@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Assunto</div>
				<div class="panel-body">
					{!! Form::model($assunto = new \App\Assunto, ['url'=>'assunto/store']) !!}
						@include ('assunto._form',['submitButtonText'=>'Adicionar registro'])
					{!! Form::close() !!}
				</div>
			</div>
			@include('errors._list')
		</div>

	</div>
</div>
@endsection
