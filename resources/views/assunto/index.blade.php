@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Lista de assunto</div>
				<div class="panel-body">
					<a href=" {{ url('/assunto/create') }}" class="btn btn-primary">Novo registro</a>
					<br />
					<br />
					<table class="table table-striped table-bordered table-hover">
					    <thead>
					        <tr>
					            <th>ID</th>
					            <th>Título</th>
					            <th>Ação</th>
					        </tr>
					    </thead>
					    @foreach($assuntos as $assunto)
			                <tr>
			                    <td>{{ $assunto->id }}</td>
			                    <td>{{ $assunto->titulo }}</td>
			                    <td>
			                        <a href="{{ URL::route('assunto.edit',['id'=>$assunto->id])  }}" class="btn btn-success">Editar</a>
			                        <a href="{{ URL::route('assunto.destroy',['id'=>$assunto->id])  }}" class="btn btn-danger">Deletar</a>
			                    </td>
			                </tr>
			            @endforeach
					</table>

					 {!! $assuntos->render() !!}

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
