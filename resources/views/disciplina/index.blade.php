@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Lista de disciplinas</div>
				<div class="panel-body">
					<a href=" {{ url('/disciplina/create') }}" class="btn btn-primary">Novo registro</a>
					<br />
					<br />
					{!! Form::select('assuntos',[null=>'Filtrar por assunto:'] + $assuntos , null, ["id"=>"assuntos",'class'=>'form-control']); !!}
					<br />
					<table class="table table-striped table-bordered table-hover">
					    <thead>
					        <tr>
					            <th>ID</th>
					            <th>Título</th>
					            <th>Assunto(s)</th>
					            <th>Ação</th>
					        </tr>
					    </thead>
					    @foreach($disciplinas as $disciplina)
			                <tr>
			                    <td>{{ $disciplina->id }}</td>
			                    <td>{{ $disciplina->titulo }}</td>
			                    <td>
			                    	@foreach($disciplina->assuntos as $assunto)
			                    		{{ $assunto->titulo }}
			                    		{!! "<br />" !!}
			                    	@endforeach
			                    </td>
			                    <td>
			                        <a href="{{ URL::route('disciplina.edit',['id'=>$disciplina->id])  }}" class="btn btn-success">Editar</a>
			                        <a href="{{ URL::route('disciplina.destroy',['id'=>$disciplina->id])  }}" class="btn btn-danger">Deletar</a>
			                    </td>
			                </tr>
			            @endforeach
					</table>

					 {!! $disciplinas->render() !!}

				</div>
			</div>
		</div>
	</div>
</div>

<script>

	$( "#assuntos" ).change(function() {
		window.location.replace("{{ url('/disciplina/') }}/"+$("#assuntos").val());
		
	});

</script>

@endsection
