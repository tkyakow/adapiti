<div class="form-group">
	{!! Form::label('titulo','Título'); !!}
	{!! Form::text('titulo',null,['class'=>'form-control']); !!}
</div>

<div class="form-group">
	{!! Form::label('assuntos_list','Assunto(s):'); !!}
	{!! Form::select('assuntos_list[]', $assuntos , null, ['id' => 'assuntos_list','class'=>'form-control','multiple']); !!}
</div>

<div class="form-group">
	{!! Form::submit($submitButtonText,['class'=>'btn btn-primary form-control']) !!}
</div>

<script>
	$('#assuntos_list').select2({
		placeholder: "Escolha um ou vários assunto(s)"
	});
</script>