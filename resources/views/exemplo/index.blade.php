@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Lista de exemplo</div>
				<div class="panel-body">
					<a href=" {{ url('/exemplo/create') }}" class="btn btn-primary">Novo registro</a>
					<br />
					<br />
					<table class="table table-striped table-bordered table-hover">
					    <thead>
					        <tr>
					            <th>ID</th>
					            <th>Título</th>
					            <th>Ação</th>
					        </tr>
					    </thead>
					    @foreach($exemplos as $exemplo)
			                <tr>
			                    <td>{{ $exemplo->id }}</td>
			                    <td>{{ $exemplo->titulo }}</td>
			                    <td>
			                        <a href="{{ URL::route('exemplo.edit',['id'=>$exemplo->id])  }}" class="btn btn-success">Editar</a>
			                        <a href="{{ URL::route('exemplo.destroy',['id'=>$exemplo->id])  }}" class="btn btn-danger">Deletar</a>
			                    </td>
			                </tr>
			            @endforeach
					</table>

					 {!! $exemplos->render() !!}

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
