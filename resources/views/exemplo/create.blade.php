@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Exemplo</div>
				<div class="panel-body">
					{!! Form::model($exemplo = new \App\Exemplo, ['url'=>'exemplo/store']) !!}
						@include ('exemplo._form',['submitButtonText'=>'Adicionar registro'])
					{!! Form::close() !!}
				</div>
			</div>
			@include('errors._list')
		</div>

	</div>
</div>
@endsection
