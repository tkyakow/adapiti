<div class="form-group">
	{!! Form::label('titulo','Título'); !!}
	{!! Form::text('titulo',null,['class'=>'form-control']); !!}
</div>
<div class="form-group">
	{!! Form::submit($submitButtonText,['class'=>'btn btn-primary form-control']) !!}
</div>
