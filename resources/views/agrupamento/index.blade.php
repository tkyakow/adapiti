@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Lista de agrupamento</div>
				<div class="panel-body">
					<a href=" {{ url('/agrupamento/create') }}" class="btn btn-primary">Novo registro</a>
					<a href=" {{ url('/agrupamento/sort') }}" class="btn btn-warning">Ordenar registro(s)</a>
					<br />
					<br />
					<table class="table table-striped table-bordered table-hover">
					    <thead>
					        <tr>
					            <th>ID</th>
					            <th>Título</th>
					            <th>Disciplina(s)</th>
					            <th>Ação</th>
					        </tr>
					    </thead>
					    @foreach($agrupamentos as $agrupamento)
			                <tr>
			                    <td>{{ $agrupamento->id }}</td>
			                    <td>{{ $agrupamento->titulo }}</td>
			                    <td>
			                    	@foreach($agrupamento->disciplinas as $disciplina)
			                    		{{ $disciplina->titulo }}
			                    		{!! "<br />" !!}
			                    	@endforeach
			                    </td>
			                    <td>
			                        <a href="{{ URL::route('agrupamento.edit',['id'=>$agrupamento->id])  }}" class="btn btn-success">Editar</a>
			                        <a href="{{ URL::route('agrupamento.destroy',['id'=>$agrupamento->id])  }}" class="btn btn-danger">Deletar</a>
			                    </td>
			                </tr>
			            @endforeach
					</table>

					 {!! $agrupamentos->render() !!}

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
