@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3>{{ $trilha->titulo }}</h3>
					<strong>Passo 2:</strong> Escolha um agrupamento
				</div>
				<div class="panel-body">
					<hr />
						{!! Form::button('Voltar', array('id' => 'voltar' , "class" => "btn btn-warning")) !!}
					<hr />
					<div class="row">
						@foreach($agrupamentos as $agrupamento)
							<div class="col-lg-4" style="min-height:200px;">
								<div class="panel panel-primary">
			                        <div class="panel-heading">
			                            {{ $agrupamento->titulo }}
			                        </div>
			                        <div class="panel-body">
			                        	<ul>
					                    	@foreach($agrupamento->disciplinas as $disciplina)
					                    		<li><a href="{{ URL::route('assunto.aberto',['id'=>$disciplina->id])  }}">{{ $disciplina->titulo }}</a></li>
					                    	@endforeach
				                    	</ul>
			                        </div>
			                    </div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$( "#voltar" ).click(function() {
	  history.back()
	});
</script>


@endsection
