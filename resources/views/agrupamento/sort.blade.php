@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Ordenação de agrupamento(s)</div>
				<div class="panel-body">
					<ul id="sortable-list">
	                	@foreach($agrupamentos as $agrupamento)
	                		<li title="{{ $agrupamento->id }}">
	                			{{ $agrupamento->titulo }}
	                			<?php $order[] = $agrupamento->id; ?>
	                		</li>
	                	@endforeach
                	</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" name="sort_order" id="sort_order" value="<?php echo implode(',',$order); ?>" />

@endsection
@section('javascript')
<script>

/* when the DOM is ready */
$(function() {
	/* grab important elements */
	var sortInput = $('#sort_order');
	var submit = $('#autoSubmit');
	var messageBox = $('#message-box');
	var list = $('#sortable-list');
	/* create requesting function to avoid duplicate code */

	var request = function() {
		$.ajax({
			beforeSend: function() {
				messageBox.text('Atualizando a ordenação na tabela.');
			},
			complete: function() {
				messageBox.text('Tabela atualizada.');
			},
			data: 'sort_order=' + sortInput[0].value, //need [0]?
			type: 'post',
			url: "{{ url('/agrupamento/sort_ajax') }}"
		});
	};
	/* worker function */
	var fnSubmit = function() {
		var sortOrder = [];

		list.children('li').each(function(){
			sortOrder.push($(this).data('id'));
		});

		sortInput.val(sortOrder.join(','));
		request();
		
	};
	/* store values */
	list.children('li').each(function() {
		var li = $(this);
		li.data('id',li.attr('title')).attr('title','');
	});
	/* sortables */
	list.sortable({
		opacity: 0.7,
		update: function() {
			fnSubmit(submit[0]);
		}
	});
	list.disableSelection();
	/* ajax form submission */
	$('#dd-form').bind('submit',function(e) {
		if(e) e.preventDefault();
		fnSubmit(true);
	});
});

</script>

@endsection