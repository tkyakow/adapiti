<div class="form-group">
	{!! Form::label('titulo','Título'); !!}
	{!! Form::text('titulo',null,['class'=>'form-control']); !!}
</div>

<div class="form-group">
	{!! Form::label('disciplina_list','Disciplina(s):'); !!}
	{!! Form::select('disciplina_list[]', $disciplinas , null, ['id' => 'disciplina_list','class'=>'form-control','multiple']); !!}
</div>

<div class="form-group">
	{!! Form::submit($submitButtonText,['class'=>'btn btn-primary form-control']) !!}
</div>

<script>
	$('#disciplina_list').select2({
		placeholder: "Escolha uma ou várias discplina(s)."
	});
</script>