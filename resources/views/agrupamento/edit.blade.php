@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Agrupamento</div>
				<div class="panel-body">
					{!! Form::model($agrupamento,['route'=>['agrupamento.update', $agrupamento->id], 'method'=>'put']) !!}
						@include ('agrupamento._form',['submitButtonText'=>'Editar registro'])
					{!! Form::close() !!}
				</div>
			</div>
			@include('errors._list')
		</div>

	</div>
</div>
@endsection
