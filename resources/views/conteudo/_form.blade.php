<div class="form-group">
	{!! Form::label('titulo','Título'); !!}
	{!! Form::text('titulo',null,['class'=>'form-control']); !!}
</div>

<div class="form-group">
	{!! Form::label('conteudo','Conteúdo'); !!}
	{!! Form::textarea('conteudo',null,['class'=>'form-control' , "id" => "editor"]); !!}
</div>

<div class="form-group">
    {!! Form::label('url_list','Identificador(es) do(s) vídeo(s)'); !!}
    <button id="opener" class="btn btn-primary" onClick="return false">Adicionar vídeo</button>
    <div id="lista_videos"></div>
</div>

<div class="form-group">
    {!! Form::label('assunto_id','Assunto que pertence esse conteúdo'); !!}
    {!! Form::select('assunto_id', $assuntos, null, array('class' => 'form-control')) !!}        
</div>

<div id="dialog" title="Vídeo">
  <p>
    Identificar do vídeo:
    <input type="text" name="url" id="url" class="form-control" required="true" />
    <button id="enviar" class="btn btn-success" style="margin-top:10px" onClick="return false">Adicionar</button>
  </p>
</div>

<script>
	$('#editor').summernote({
      toolbar: [
        //[groupname, [button list]]
         
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
      ],height: 300
    });
    $('#url_list').select2({
        tags: true
    });

    $(function() {
        $( "#dialog" ).dialog({
          autoOpen: false
        });

        $( "#opener" ).click(function() {
          $( "#dialog" ).dialog( "open" );
        });
    });

    $( "#enviar" ).click(function() {
        $.ajax({
            beforeSend: function() {
                $("#enviar").hide();
            },
            complete: function() {
                $("#enviar").show();
                $( "#dialog" ).dialog( "close" );
            },
            data: { url : $("#url").val() , conteudo_id : <?php echo $conteudo->id; ?>},
            type: 'post',
            url: "{{ url('/conteudo/adicionar_video') }}",
            success: function(data) { 
                var html = '';
                for($i=0; $i < data.length; $i++){
                    html += "<h6>"+data[$i].url+" <button class='btn btn-danger' onClick='javascript:deletar_video("+data[$i].id+")'>x</button> </h6>";
                    html += '<hr />';
                    $('#lista_videos').html(html);  
                }
            }
        });
    });

    function deletar_video(id){
        $.ajax({
            beforeSend: function() {
                $("#enviar").hide();
                $('#lista_videos').html("");
            },
            complete: function() {
                $("#enviar").show();
                $( "#dialog" ).dialog( "close" );
            },
            data: { id : id , conteudo_id :<?php echo $conteudo->id; ?>},
            type: 'post',
            url: "{{ url('/conteudo/deletar_video') }}",
            success: function(data) { 
                var html = '';
                for($i=0; $i < data.length; $i++){
                    html += "<h6>"+data[$i].url+" <button class='btn btn-danger' onClick='javascript:deletar_video("+data[$i].id+")'>x</button> </h6>";
                    html += '<hr />';
                    $('#lista_videos').html(html);  
                }
            }
        });
    }
</script>