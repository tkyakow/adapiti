@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Conteúdo</div>
				<div class="panel-body">
					{!! Form::model($conteudo,['route'=>['conteudo.update', $conteudo->id], 'method'=>'put']) !!}
						@include ('conteudo._form')

						<hr />

						<span class="btn btn-success fileinput-button">
						    <i class="glyphicon glyphicon-plus"></i>
						    <span>Selecionar arquivos...</span>
						    <!-- The file input field used as target for the file upload widget -->
						    <input id="fileupload" type="file" name="arquivo" data-token="{!! csrf_token() !!}">
						</span>
						<br />
						<br />
						<!-- The global progress bar -->
						<div id="progress" class="progress">
						    <div class="progress-bar progress-bar-success"></div>
						</div>

						<table class="table table-bordered table-striped table-hover">
						    <thead>
						        <tr>
						            <th>Nome</th>
						            <th>Enviado em</th>
						            <th>Ações</th>
						        </tr>
						    </thead>
						    <tbody>
						        @foreach($conteudo->files as $file)
						        <tr>
						            <td>{!! $file->name !!}</td>
						            <td>{!! $file->created_at !!}</td>
						            <td>
						                <a href="{!! route('files.destroy', [$conteudo->id, $file->id]) !!}" class="btn btn-danger fileinput-button">Deletar</a>
						            </td>
						        </tr>
						        @endforeach
						    </tbody>
						</table>

						<hr />

						<div class="form-group">
						    {!! Form::submit('Editar registro',['class'=>'btn btn-primary form-control']) !!}
						</div>


					{!! Form::close() !!}
				</div>
			</div>
			@include('errors._list')
		</div>

		<script>
		     $( document ).ready(function() {
		        $('#fileupload').fileupload({
		            url: '/upload',
		            dataType: 'JSON',
		            formData: {_token: $("#fileupload").data('token') , conteudoId: {{ $conteudo->id }} },
		            type: 'POST',
		            progressall: function (e, data) {
		                var progress = parseInt(data.loaded / data.total * 100, 10);
		                $('#progress .progress-bar').css(
		                    'width',
		                    progress + '%'
		                );

		                if(data.loaded == data.total){
		                    location.reload(); 
		                }
		            }
		        });
		     });
		</script>

		<style>
		 #editor {overflow:scroll; max-height:300px}
		</style>

	</div>
</div>

@endsection
