@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Lista de conteúdo</div>
				<div class="panel-body">
					<a href=" {{ url('/conteudo/create') }}" class="btn btn-primary">Novo registro</a>
					<br />
					<br />
					{!! Form::select('assuntos',[null=>'Filtrar por assunto:'] + $assuntos , null, ["id"=>"assuntos",'class'=>'form-control']); !!}
					<br />
					<table class="table table-striped table-bordered table-hover">
					    <thead>
					        <tr>
					            <th>ID</th>
					            <th>Título</th>
					            <th>Vídeos</th>
					            <th>Assunto</th>
					            <th>Ação</th>
					        </tr>
					    </thead>
					    @foreach($conteudos as $conteudo)
			                <tr>
			                    <td>{{ $conteudo->id }}</td>
			                    <td>{{ $conteudo->titulo }}</td>
			                    <td>
			                    	@foreach($conteudo->videos as $video)
			                    		<a href='http://player.vimeo.com/video/80629469' data-toggle="lightbox">{{ $video->url }}</a>
			                    		{!! "<br />" !!}
			                    	@endforeach
			                    </td>
			                    <td>{{ $conteudo->assuntos->titulo }}</td>
			                    <td>
			                        <a href="{{ URL::route('conteudo.edit',['id'=>$conteudo->id])  }}" class="btn btn-success">Editar</a>
			                        <a href="{{ URL::route('conteudo.destroy',['id'=>$conteudo->id])  }}" class="btn btn-danger">Deletar</a>
			                    </td>
			                </tr>
			            @endforeach
					</table>

					 {!! $conteudos->render() !!}

					 <script>
						$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
						    event.preventDefault();
						    $(this).ekkoLightbox();
						}); 
					 </script>

				</div>
			</div>
		</div>
	</div>
</div>

<script>

	$( "#assuntos" ).change(function() {
		window.location.replace("{{ url('/conteudo/') }}/"+$("#assuntos").val());
		
	});

</script>

@endsection
