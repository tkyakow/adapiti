@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading"></div>
				<div class="panel-body">
					<hr />
						{!! Form::button('Voltar', array('id' => 'voltar' , "class" => "btn btn-warning")) !!}
					<hr />
					<div class="row">
						<div class="panel-body">

							<div class="panel panel-default">
		                        <div class="panel-heading">
		                        	Conteúdo didático
		                        </div>
		                        <div class="panel-body">
				                    <h2>{{ $conteudos->titulo }}</h2>
									{!! $conteudos->conteudo !!}
		                        </div>
		                    </div>

							<div class="panel panel-default">
		                        <div class="panel-heading">
		                        	Vídeos
		                        </div>
		                        <div class="panel-body">
			                    	@foreach($conteudos->videos as $video)
			                    		<div class="col-lg-6">
			                    			<iframe height="314.71999999999997" frameborder="0" width="100%" allowfullscreenclass="embed-responsive-item" src="http://player.vimeo.com/video/{{ $video->url }}?autoplay=0"></iframe>
			                    		</div>
			                    	@endforeach
		                        </div>
		                    </div>

							<div class="panel panel-default">
		                        <div class="panel-heading">
		                        	Arquivos
		                        </div>
		                        <div class="panel-body">
			                    	@foreach($conteudos->files as $file)
			                    		<div class="col-lg-6">
			                    			<a class="btn btn-block btn-success btn-vk">
				                                <i class="fa fa-file"></i> {{ $file->name }}
				                            </a>
			                    		</div>
			                    	@endforeach
		                        </div>
		                    </div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$( "#voltar" ).click(function() {
	  history.back()
	});
</script>


@endsection
