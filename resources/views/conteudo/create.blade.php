@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Conteúdo</div>
				<div class="panel-body">
					{!! Form::model($conteudo = new \App\Conteudo, ['url'=>'conteudo/store']) !!}
						@include ('conteudo._form')

						<div class="form-group">
						    {!! Form::submit('Próximo passo >',['class'=>'btn btn-primary form-control']) !!}
						</div>

					{!! Form::close() !!}
				</div>
			</div>
			@include('errors._list')
		</div>
	</div>
</div>
@endsection
