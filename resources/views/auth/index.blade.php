@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Lista de usuários</div>
				<div class="panel-body">

					<table class="table table-striped table-bordered table-hover">
					    <thead>
					        <tr>
					            <th>ID</th>
					            <th>Nome</th>
					            <th>Descrição</th>
					            <th>Ação</th>
					        </tr>
					    </thead>
					    @foreach($users as $u)
			                <tr>
			                    <td>{{ $p->id }}</td>
			                    <td>{{ $p->name }}</td>
			                </tr>
			            @endforeach
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
