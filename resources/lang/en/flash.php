<?php
return [
    'cadastrado'                    => 'Registro cadastrado com sucesso.',
    'atualizado'                    => 'Registro atualizado com sucesso.',
    'deletado'                      => 'Registro deletado com sucesso.',
    
    'cadastrado_erro'               => 'O registro não foi cadastrado.',
    'atualizado_erro'               => 'O registro não foi atualizado.',
    'deletado_erro'                 => 'O registro não foi deletado.',
];